

#NEWINGHENT



##Groepsleden
--------------------------------
* Andries Meyers

##Briefing & Analyse

‘New In Ghent’ is een handige applicatie die zich richt op mensen die net in Gent zijn komen wonen. De gebruiker vindt op een gemakkelijke manier de leukste plekjes bij hem in de buurt. Zo leert hij zijn omgeving beter kennen, en integreert hij zich makkelijker in de stad. De informatie moet relevant zijn, en moet het onderwerp voldoende beschrijven. De applicatie wordt op een gebruiksvriendelijke manier gepresenteerd, zodat de gebruiker gemakkelijk zijn weg vindt binnen de verschillende categorieën. Dit kan variëren van de dichtstbijzijnde restaurants tot de leukste evenementen.

##Technische specificaties

----------------------------------
###Webtechnologieën

 - HTML 5
 - CSS 3
 - JSON
 - Javascript
 - jQuery
 - Handlebars

###Gebruikte datasets:

 - VisitGent Events
   (http://datatank.stad.gent/4/toerisme/visitgentevents.json) 
 - VisitGent Spots
   (http://datatank.stad.gent/4/toerisme/visitgentspots.json) 
 - Restorestjes
   (http://datatank.stad.gent/4/milieuennatuur/restorestjes.json) 
 - Nieuwsberichten
   (http://datatank.stad.gent/4/bestuurenbeleid/linkedopendatanieuwsberichten.json)
 - Weersomstandigheden
   (http://datatank.stad.gent/4/milieuennatuur/weersomstandigheden.json)
 - Kotatgent (http://datatank.stad.gent/4/wonen/kotatgent.json) 
 - Buurtsportlocaties
   (http://datatank.stad.gent/4/cultuursportvrijetijd/buurtsportlocaties.json)

##Persona's

----------------------------------
![Tom](images/persona_tom.PNG "Tom")
**Tom** – Tom is een toekomstgerichte zakenman die met zijn partner het ouderlijke huis heeft verlaten om een eigen stekje te huren in Gent. Hij is een echte fijnproever, maar omdat hij de stad niet echt kent, weet hij niet waar de beste restaurants zich bevinden. Via onze webapplicatie weet hij in no time waar hij heen moet.

----------------------------------

![Emma](images/persona_emma.PNG "Emma")
**Emma** - Emma is een enthousiaste werkneemster die onlangs een carrièreswitch gemaakt heeft. Hierdoor is ze echter moeten verhuizen naar het urbane Gent. Om van alle stress te onstnappen, gaat ze regelmatig gaan joggen. Omdat ze een druk schema heeft, neemt ze vlug een kijkje op onze app om te weten te komen wat voor weer haar te wachten staat. En als het weer niet mee zit, kan ze nog steeds de plaatselijke sporthal vinden met onze applicatie.

-----------------------------------------------

![Philippe](images/persona_philippe.PNG "Philippe")
**Philippe** – Philippe is een gerenomeerd manager die door een headhunter weggeplukt is uit Brussel. In België staat hij aan het hoofd van een grote onderneming. Zijn integratie in Gent verloopt moeizaam, omdat hij de stad niet kent. Om dit te vergemakkelijken, gaat hij via onze app op zoek naar de leukste plekjes en de tofste evenementen.

----------------------------------

![Misao](images/persona_misao.PNG "Misao")
**Misao** – Misao is een gepensioneerde vrouw die met haar familie verhuisd is naar Gent. Omdat ze vaak alleen thuis zit, is ze op zoek naar een manier om zich bezig te houden. Dankzij New In Ghent blijft ze op de hoogte van de laatste nieuwtjes in Gent. Daarnaast spendeert ze graag haar tijd door de bezienswaardigheden in de stad te bezoeken. Die vindt ze, dankzij de gebruiksvriendelijkheid van onze app, zeer gemakkelijk terug.


##Ideëenbord

----------------------------------
![Ideënbord1](ontwerpen/Ideaboard.png "Ideëenbord")


##Moodboard

----------------------------------
![Moodboard](ontwerpen/Moodboard.png "Moodboard")

##Sitemap

----------------------------------

![Sitemap](ontwerpen/Sitemap.png "Sitemap")

##Wireflow

----------------------------------

![Wireflow](ontwerpen/WireF.png "Wireflow")

##Styletile

----------------------------------
![Styletile](ontwerpen/StyleTile.png "Styletile")

##Visual Design

----------------------------------
![Visual Design](ontwerpen/VisualDesigns.png "Visual Design")


##Screenshot Snippets

----------------------------------

####HTML
![Screenshot HTML](images/screenshot_html1.png "Screenshot HTML")

####CSS
![Screenshot CSS](images/screenshot_css.png "Screenshot CSS")

####Javascript
![Screenshot Javascript](images/screenshot_js1.png "Screenshot Javascript 1")
![Screenshot Javascript](images/screenshot_js2.png "Screenshot Javascript 2")
![Screenshot Javascript](images/screenshot_js3.png "Screenshot Javascript 3")


##Screenshot Eindresultaat

----------------------------------
#### Mobile
![Eindresultaat](images/screenshot_eindresultaat_mb1.png "Eindresultaat")
![Eindresultaat](images/screenshot_eindresultaat_mb2.png "Eindresultaat")
![Eindresultaat](images/screenshot_eindresultaat_mb3.png "Eindresultaat")

#### Desktop
![Eindresultaat](images/screenshot_eindresultaat_desk1.png "Eindresultaat")
![Eindresultaat](images/screenshot_eindresultaat_desk2.png "Eindresultaat")
![Eindresultaat](images/screenshot_eindresultaat_desk3.png "Eindresultaat")

